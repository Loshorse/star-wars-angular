import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanetsComponent } from './planets/planets.component';
import { PlanetsDetailsComponent } from './planets-details/planets-details.component';

const routes: Routes = [
  { path: 'details', component: PlanetsDetailsComponent },
  { path: '', component: PlanetsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanetsRoutingModule {}

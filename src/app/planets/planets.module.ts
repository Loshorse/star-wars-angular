import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetsComponent } from './planets/planets.component';
import { PlanetsDetailsComponent } from './planets-details/planets-details.component';

import { PlanetsRoutingModule } from './planets-routing.module';

import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [PlanetsComponent, PlanetsDetailsComponent],
  imports: [CommonModule, SharedModule, PlanetsRoutingModule]
})
export class PlanetsModule {}

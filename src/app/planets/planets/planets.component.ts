import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../app/services/api.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent implements OnInit {
  planets: any[];
  planetsTotal: number;
  totalItems: number;
  page: Number = 1;
  url: string;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.getPlanets(1);
  }

  getPlanets(page: number) {
    this.url = `https://swapi.co/api/planets/?page=${page}`;

    this.apiService.getViaSwUrl(this.url).subscribe(
      (planets) => {
        this.planets = planets.results;

        this.totalItems = planets.count;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}

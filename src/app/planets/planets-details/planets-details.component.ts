import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../app/services/api.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-planets-details',
  templateUrl: './planets-details.component.html',
  styleUrls: ['./planets-details.component.scss']
})
export class PlanetsDetailsComponent implements OnInit {
  planet: any;
  planetsId;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.planetsId = this.route.snapshot.queryParams['url'];

    this.getPlanetsById();
  }

  getPlanetsById() {
    this.apiService.getViaSwUrl(`${this.planetsId}`).subscribe(
      (planet) => {
        this.planet = planet;

        console.log(this.planet);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}

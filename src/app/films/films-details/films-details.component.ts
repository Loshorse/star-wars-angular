import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../app/services/api.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-films-details',
  templateUrl: './films-details.component.html',
  styleUrls: ['./films-details.component.scss']
})
export class FilmsDetailsComponent implements OnInit {
  film: any;
  filmId;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.filmId = this.route.snapshot.params['id'];

    this.getFilmById();
  }

  getFilmById() {
    this.apiService.get(`films/${this.filmId}`).subscribe(
      (film) => {
        this.film = film;

        console.log(this.film);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../app/services/api.service';

import { slideAnimation } from '../../animations/animations';


@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {
  films: any[];

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.getFilms();
  }

  getFilms() {
    this.apiService.get('films').subscribe(
      (films) => {
        this.films = films.results;

        console.log(this.films);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilmsDetailsComponent } from './films-details/films-details.component';
import { FilmsComponent } from './films/films.component';

import { FilmsRoutingModule } from './films-routing.module';

import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [FilmsDetailsComponent, FilmsComponent],
  imports: [CommonModule, SharedModule, FilmsRoutingModule]
})
export class FilmsModule {}

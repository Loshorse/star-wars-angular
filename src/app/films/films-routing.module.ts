import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FilmsComponent } from './films/films.component';
import { FilmsDetailsComponent } from './films-details/films-details.component';

const routes: Routes = [
  { path: ':id', component: FilmsDetailsComponent },
  { path: '', component: FilmsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilmsRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { MesssageComponent } from './messsage/messsage.component';

@NgModule({
  declarations: [MesssageComponent],
  exports: [NgbPaginationModule],
  imports: [CommonModule, NgbPaginationModule]
})
export class SharedModule {}

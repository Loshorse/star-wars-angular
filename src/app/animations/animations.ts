import { trigger, transition, useAnimation } from '@angular/animations';

import { zoomIn } from 'ngx-animate';

export const slideAnimation = trigger('bounce', [
  transition('* => *', useAnimation(zoomIn))
]);
import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../app/services/api.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {
  people: any[];
  peopleTotal: number;
  totalItems: number;
  page = 1;
  url: any;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.getPeople(1);
  }

  getPeople(page: number) {
    this.url = `https://swapi.co/api/people/?page=${this.page}`;
    this.apiService
      .getViaSwUrl(this.url)
      .subscribe(
        (people) => {
          console.log(people);
          this.people = people.results;

          this.totalItems = people.count;
        },
        (error) => {
          console.log(error);
        }
      );
  }

}

import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../app/services/api.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-people-details',
  templateUrl: './people-details.component.html',
  styleUrls: ['./people-details.component.scss']
})
export class PeopleDetailsComponent implements OnInit {
  people: any;
  peopleId;
  url: string;
  homeworld;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.peopleId = this.route.snapshot.queryParams['url'];

    this.getPeopleById();
  }

  getPeopleById() {
    this.apiService.getViaSwUrl(`${this.peopleId}`).subscribe(
      (people) => {
        this.people = people;

        console.log(this.people);

        this.getPeopleHomeWorld()
      },
      (error) => {
        console.log(error);
      }
    );
  }

  
  getPeopleHomeWorld() {
    this.url = this.people.homeworld;

    this.apiService
    .getViaSwUrl(this.url)
    .subscribe(
      (planet) => {
        console.log(planet);
        this.people.homeworld = planet.name;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}

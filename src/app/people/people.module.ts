import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleComponent } from './people/people.component';
import { PeopleDetailsComponent } from './people-details/people-details.component';

import { PeopleRoutingModule } from './people-routing.module';

import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PeopleComponent, PeopleDetailsComponent],
  imports: [CommonModule, SharedModule, PeopleRoutingModule]
})
export class PeopleModule {}

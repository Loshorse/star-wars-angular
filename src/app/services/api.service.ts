import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API: string;
  errorMessage: string;

  constructor(private http: HttpClient) {
    this.API = 'https://swapi.co/api/';
  }

  //GET ROUTE WITHOUT TOKEN
  get(apiEndpoint: string): Observable<any> {
    return this.http
      .get(this.API + apiEndpoint)
      .pipe(catchError(this.handleError));
  }

  getViaSwUrl(apiEndpoint: string): Observable<any> {
    return this.http.get(apiEndpoint).pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    console.error(error);
    this.errorMessage = error.error;

    // return an observable with a user-facing error message
    return throwError(this.errorMessage);
  }
}

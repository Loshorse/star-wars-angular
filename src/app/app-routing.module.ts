import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MesssageComponent } from './shared/messsage/messsage.component';


const routes: Routes = [
  {
    path: '',
    component: MesssageComponent
  },
  
  {
    path: 'films',
    loadChildren: () => import('./films/films.module').then(m => m.FilmsModule) 
  },

  {
    path: 'people',
    loadChildren: () => import('./people/people.module').then(m => m.PeopleModule) 
  },

  {
    path: 'planets',
    loadChildren: () => import('./planets/planets.module').then(m => m.PlanetsModule) 
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
